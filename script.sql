USE [master]
GO
/****** Object:  Database [Quizlett]    Script Date: 3/25/2023 6:07:45 PM ******/
CREATE DATABASE [Quizlett]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Quizlett', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Quizlett.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Quizlett_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\Quizlett_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Quizlett] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Quizlett].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Quizlett] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Quizlett] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Quizlett] SET ARITHABORT OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Quizlett] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Quizlett] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Quizlett] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Quizlett] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Quizlett] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Quizlett] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Quizlett] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Quizlett] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Quizlett] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Quizlett] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Quizlett] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Quizlett] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Quizlett] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Quizlett] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Quizlett] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Quizlett] SET RECOVERY FULL 
GO
ALTER DATABASE [Quizlett] SET  MULTI_USER 
GO
ALTER DATABASE [Quizlett] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Quizlett] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Quizlett] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Quizlett] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Quizlett] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Quizlett] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Quizlett', N'ON'
GO
ALTER DATABASE [Quizlett] SET QUERY_STORE = OFF
GO
USE [Quizlett]
GO
/****** Object:  Table [dbo].[Answers]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Answers](
	[AnswerID] [int] IDENTITY(1,1) NOT NULL,
	[Answer] [varchar](max) NOT NULL,
	[ExID] [int] NOT NULL,
 CONSTRAINT [PK_Answers] PRIMARY KEY CLUSTERED 
(
	[AnswerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[ISBN] [nvarchar](20) NOT NULL,
	[Title] [varchar](max) NOT NULL,
	[Edition] [varchar](max) NOT NULL,
	[Authors] [varchar](max) NOT NULL,
	[Image] [varchar](max) NOT NULL,
	[NumOfAnswers] [varchar](max) NOT NULL,
	[CateID] [int] NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[ISBN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Card]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Card](
	[cardId] [int] IDENTITY(1,1) NOT NULL,
	[term] [nvarchar](max) NULL,
	[definition] [nvarchar](max) NULL,
	[studySetId] [int] NULL,
 CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[cardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CateID] [int] IDENTITY(1,1) NOT NULL,
	[CateName] [varchar](max) NOT NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[CateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Chapter]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chapter](
	[ChapterID] [int] NOT NULL,
	[ChapterName] [varchar](max) NOT NULL,
	[ISBN] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Chapter] PRIMARY KEY CLUSTERED 
(
	[ChapterID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Class]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[classId] [int] IDENTITY(1,1) NOT NULL,
	[className] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[isInvite] [bit] NULL,
	[inviteCode] [nchar](10) NULL,
	[isEdit] [bit] NULL,
	[schoolName] [nvarchar](max) NULL,
	[userId] [int] NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[classId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Exercises]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exercises](
	[ExID] [int] IDENTITY(1,1) NOT NULL,
	[ExName] [varchar](max) NOT NULL,
	[Answers] [varchar](max) NOT NULL,
	[PageID] [int] NOT NULL,
 CONSTRAINT [PK_Exercises] PRIMARY KEY CLUSTERED 
(
	[ExID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FlashCards]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FlashCards](
	[userId] [int] NOT NULL,
	[cardId] [int] NOT NULL,
	[studySetId] [int] NOT NULL,
	[flashCardId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_FlashCards] PRIMARY KEY CLUSTERED 
(
	[flashCardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[folderId] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[userId] [int] NULL,
	[isShare] [bit] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[folderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LearnCards]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LearnCards](
	[learnId] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NOT NULL,
	[cardId] [int] NOT NULL,
	[studySetId] [int] NOT NULL,
 CONSTRAINT [PK_LearnCards] PRIMARY KEY CLUSTERED 
(
	[learnId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListClass]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListClass](
	[listClassId] [int] IDENTITY(1,1) NOT NULL,
	[classId] [int] NOT NULL,
	[folderId] [int] NOT NULL,
 CONSTRAINT [PK_ListClass] PRIMARY KEY CLUSTERED 
(
	[listClassId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListEditId]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListEditId](
	[studySetId] [int] NULL,
	[userId] [int] NULL,
	[listEditId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListEditId] PRIMARY KEY CLUSTERED 
(
	[listEditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListFolder]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListFolder](
	[studySetId] [int] NULL,
	[folderId] [int] NULL,
	[ListFolderId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListFolder] PRIMARY KEY CLUSTERED 
(
	[ListFolderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListMember]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListMember](
	[classId] [int] NULL,
	[userId] [int] NULL,
	[listMemberId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListMember] PRIMARY KEY CLUSTERED 
(
	[listMemberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ListStudySet]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ListStudySet](
	[classId] [int] NULL,
	[studySetId] [int] NULL,
	[ListStudySetId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_ListStudySet] PRIMARY KEY CLUSTERED 
(
	[ListStudySetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Page]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[PageID] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [varchar](max) NOT NULL,
	[ChapterID] [int] NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[PageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StudySet]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StudySet](
	[studySetId] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[isShare] [bit] NULL,
	[userId] [int] NULL,
 CONSTRAINT [PK_StudySet] PRIMARY KEY CLUSTERED 
(
	[studySetId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 3/25/2023 6:07:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[name] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[gmail] [nvarchar](50) NULL,
	[isActive] [bit] NULL,
	[avatar] [nvarchar](max) NULL,
	[userId] [int] IDENTITY(1,1) NOT NULL,
	[language] [nvarchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[userId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Class] ADD  CONSTRAINT [DF_Class_isInvite]  DEFAULT ((0)) FOR [isInvite]
GO
ALTER TABLE [dbo].[Class] ADD  CONSTRAINT [DF_Class_isEdit]  DEFAULT ((0)) FOR [isEdit]
GO
ALTER TABLE [dbo].[Folder] ADD  CONSTRAINT [DF_Folder_isShare]  DEFAULT ((1)) FOR [isShare]
GO
ALTER TABLE [dbo].[StudySet] ADD  CONSTRAINT [DF_StudySet_isShare]  DEFAULT ((1)) FOR [isShare]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_isActive]  DEFAULT ((0)) FOR [isActive]
GO
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_language]  DEFAULT (N'VI') FOR [language]
GO
ALTER TABLE [dbo].[Answers]  WITH CHECK ADD  CONSTRAINT [FK_Answers_Exercises] FOREIGN KEY([ExID])
REFERENCES [dbo].[Exercises] ([ExID])
GO
ALTER TABLE [dbo].[Answers] CHECK CONSTRAINT [FK_Answers_Exercises]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Category] FOREIGN KEY([CateID])
REFERENCES [dbo].[Category] ([CateID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Category]
GO
ALTER TABLE [dbo].[Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[Card] CHECK CONSTRAINT [FK_Card_StudySet]
GO
ALTER TABLE [dbo].[Chapter]  WITH CHECK ADD  CONSTRAINT [FK_Chapter_Book] FOREIGN KEY([ISBN])
REFERENCES [dbo].[Book] ([ISBN])
GO
ALTER TABLE [dbo].[Chapter] CHECK CONSTRAINT [FK_Chapter_Book]
GO
ALTER TABLE [dbo].[Class]  WITH CHECK ADD  CONSTRAINT [FK_Class_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[Class] CHECK CONSTRAINT [FK_Class_User]
GO
ALTER TABLE [dbo].[Exercises]  WITH CHECK ADD  CONSTRAINT [FK_Exercises_Page] FOREIGN KEY([PageID])
REFERENCES [dbo].[Page] ([PageID])
GO
ALTER TABLE [dbo].[Exercises] CHECK CONSTRAINT [FK_Exercises_Page]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_Card] FOREIGN KEY([cardId])
REFERENCES [dbo].[Card] ([cardId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_Card]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_StudySet]
GO
ALTER TABLE [dbo].[FlashCards]  WITH CHECK ADD  CONSTRAINT [FK_FlashCards_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[FlashCards] CHECK CONSTRAINT [FK_FlashCards_User]
GO
ALTER TABLE [dbo].[Folder]  WITH CHECK ADD  CONSTRAINT [FK_Folder_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[Folder] CHECK CONSTRAINT [FK_Folder_User]
GO
ALTER TABLE [dbo].[LearnCards]  WITH CHECK ADD  CONSTRAINT [FK_LearnCards_Card] FOREIGN KEY([cardId])
REFERENCES [dbo].[Card] ([cardId])
GO
ALTER TABLE [dbo].[LearnCards] CHECK CONSTRAINT [FK_LearnCards_Card]
GO
ALTER TABLE [dbo].[LearnCards]  WITH CHECK ADD  CONSTRAINT [FK_LearnCards_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[LearnCards] CHECK CONSTRAINT [FK_LearnCards_User]
GO
ALTER TABLE [dbo].[ListClass]  WITH CHECK ADD  CONSTRAINT [FK_ListClass_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListClass] CHECK CONSTRAINT [FK_ListClass_Class]
GO
ALTER TABLE [dbo].[ListClass]  WITH CHECK ADD  CONSTRAINT [FK_ListClass_Folder] FOREIGN KEY([folderId])
REFERENCES [dbo].[Folder] ([folderId])
GO
ALTER TABLE [dbo].[ListClass] CHECK CONSTRAINT [FK_ListClass_Folder]
GO
ALTER TABLE [dbo].[ListEditId]  WITH CHECK ADD  CONSTRAINT [FK_ListEditId_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListEditId] CHECK CONSTRAINT [FK_ListEditId_StudySet]
GO
ALTER TABLE [dbo].[ListEditId]  WITH CHECK ADD  CONSTRAINT [FK_ListEditId_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[ListEditId] CHECK CONSTRAINT [FK_ListEditId_User]
GO
ALTER TABLE [dbo].[ListFolder]  WITH CHECK ADD  CONSTRAINT [FK_ListFolder_Folder] FOREIGN KEY([folderId])
REFERENCES [dbo].[Folder] ([folderId])
GO
ALTER TABLE [dbo].[ListFolder] CHECK CONSTRAINT [FK_ListFolder_Folder]
GO
ALTER TABLE [dbo].[ListFolder]  WITH CHECK ADD  CONSTRAINT [FK_ListFolder_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListFolder] CHECK CONSTRAINT [FK_ListFolder_StudySet]
GO
ALTER TABLE [dbo].[ListMember]  WITH CHECK ADD  CONSTRAINT [FK_ListMember_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListMember] CHECK CONSTRAINT [FK_ListMember_Class]
GO
ALTER TABLE [dbo].[ListMember]  WITH CHECK ADD  CONSTRAINT [FK_ListMember_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[ListMember] CHECK CONSTRAINT [FK_ListMember_User]
GO
ALTER TABLE [dbo].[ListStudySet]  WITH CHECK ADD  CONSTRAINT [FK_ListStudySet_Class] FOREIGN KEY([classId])
REFERENCES [dbo].[Class] ([classId])
GO
ALTER TABLE [dbo].[ListStudySet] CHECK CONSTRAINT [FK_ListStudySet_Class]
GO
ALTER TABLE [dbo].[ListStudySet]  WITH CHECK ADD  CONSTRAINT [FK_ListStudySet_StudySet] FOREIGN KEY([studySetId])
REFERENCES [dbo].[StudySet] ([studySetId])
GO
ALTER TABLE [dbo].[ListStudySet] CHECK CONSTRAINT [FK_ListStudySet_StudySet]
GO
ALTER TABLE [dbo].[Page]  WITH CHECK ADD  CONSTRAINT [FK_Page_Chapter] FOREIGN KEY([ChapterID])
REFERENCES [dbo].[Chapter] ([ChapterID])
GO
ALTER TABLE [dbo].[Page] CHECK CONSTRAINT [FK_Page_Chapter]
GO
ALTER TABLE [dbo].[StudySet]  WITH CHECK ADD  CONSTRAINT [FK_StudySet_User] FOREIGN KEY([userId])
REFERENCES [dbo].[User] ([userId])
GO
ALTER TABLE [dbo].[StudySet] CHECK CONSTRAINT [FK_StudySet_User]
GO
USE [master]
GO
ALTER DATABASE [Quizlett] SET  READ_WRITE 
GO
