﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentsManagement.Models;

namespace StudentsManagement.Models
{
    public class StudentDAO
    {
        private static StudentDAO instance = null;
        private static readonly object instanceLock = new object();
        public static StudentDAO Instance
        {
            get
            {
                lock (instanceLock)
                {
                    if (instance == null)
                    {
                        instance = new StudentDAO();
                    }
                    return instance;
                }
            }
        }

        public IEnumerable<Student> GetStudentList()
        {
            var students = new List<Student>();
            try
            {
                using var context = new StudentsContext();
                students = context.Students.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return students;
        }

        public Student GetStudentByID(int studentID)
        {
            Student student = null;
            try
            {
                using var context = new StudentsContext();
                student = context.Students.FirstOrDefault(s => s.Id == studentID);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return student;
        }

        public void AddNew(Student student)
        {
            try
            {
                Student _student = GetStudentByID(student.Id);
                if (_student == null)
                {
                    using var context = new StudentsContext();
                    context.Students.Add(student);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("The student is already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Update(Student student)
        {
            try
            {
                Student _student = GetStudentByID(student.Id);
                if (_student != null)
                {
                    using var context = new StudentsContext();
                    context.Students.Update(student);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("The student does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void Remove(int sID)
        {
            try
            {
                Student student = GetStudentByID(sID);
                if (student != null)
                {
                    using var context = new StudentsContext();
                    context.Students.Remove(student);
                    context.SaveChanges();
                }
                else
                {
                    throw new Exception("The student does not already exist.");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
