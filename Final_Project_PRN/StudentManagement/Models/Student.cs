﻿using System;
using System.Collections.Generic;

namespace StudentsManagement.Models;

public partial class Student
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public int? Age { get; set; }

    public string? Address { get; set; }

    public double? Avg { get; set; }

    public int? AdmissionalYear { get; set; }

    public string? Majors { get; set; }

    public string? Type { get; set; }
}
