using Microsoft.VisualBasic.Devices;
using StudentsManagement.Models;
using StudentsManagement.Repository;
using System.Windows.Forms;

namespace StudentManagement
{
    public partial class Form1 : Form
    {
        StudentsContext db = new StudentsContext();
        IStudentRepository studentRepository = new StudentRepository();
        BindingSource source;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadStudentList();
        }

        private void LoadStudentList()
        {
            var student = studentRepository.GetStudents();
            try
            {
                //The BindingSource component is designed to simplify
                //the process of binding controls to an underlying data source
                source = new BindingSource();
                source.DataSource = student;

                dgvStudent.DataSource = null;
                dgvStudent.DataSource = student;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load student list");
            }
        }

        private void dgvStudent_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txtID.Text = dgvStudent.CurrentRow.Cells[0].Value.ToString();
            txtName.Text = dgvStudent.CurrentRow.Cells[1].Value.ToString();
            txtAge.Text = dgvStudent.CurrentRow.Cells[2].Value.ToString();
            txtAddress.Text = dgvStudent.CurrentRow.Cells[3].Value.ToString();
            txtAvgMark.Text = dgvStudent.CurrentRow.Cells[4].Value.ToString();
            msbYear.Text = dgvStudent.CurrentRow.Cells[5].Value.ToString();
            txtMajors.Text = dgvStudent.CurrentRow.Cells[6].Value.ToString();
            txtType.Text = dgvStudent.CurrentRow.Cells[7].Value.ToString();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Name = txtName.Text;
            s.Age = int.Parse(txtAge.Text);
            s.Address = txtAddress.Text;
            s.Avg = double.Parse(txtAvgMark.Text);
            if (msbYear.Text.Length < 4 || msbYear.Text.Length > 4)
            {
                MessageBox.Show("This field must be filled 4 characters.");
            }
            else
            {
                s.AdmissionalYear = int.Parse(msbYear.Text);
            }
            s.Majors = txtMajors.Text;

            if (s.Avg > 0 && s.Avg <= 4)
            {
                txtType.Text = "Yeu";
                s.Type = "Yeu";
            }
            else if (s.Avg > 4 && s.Avg <= 6)
            {
                txtType.Text = "TB";
                s.Type = "TB";

            }
            else if (s.Avg > 6 && s.Avg < 8)
            {
                txtType.Text = "Kha";
                s.Type = "Kha";
            }
            else if (s.Avg >= 8 && s.Avg <= 10)
            {
                txtType.Text = "Gioi";
                s.Type = "Gioi";
            }
            else
            {
                MessageBox.Show("Invalid");
            }
            studentRepository.InsertStudent(s);
            LoadStudentList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Id = int.Parse(txtID.Text);
            s.Name = txtName.Text;
            s.Age = int.Parse(txtAge.Text);
            s.Address = txtAddress.Text;
            s.Avg = double.Parse(txtAvgMark.Text);
            if (msbYear.Text.Length < 4 || msbYear.Text.Length > 4)
            {
                MessageBox.Show("This field must be filled 4 characters.");
            }
            else
            {
                s.AdmissionalYear = int.Parse(msbYear.Text);
            }
            s.Majors = txtMajors.Text;

            if (s.Avg > 0 && s.Avg <= 4)
            {
                txtType.Text = "Yeu";
                s.Type = "Yeu";
            }
            else if (s.Avg > 4 && s.Avg <= 6)
            {
                txtType.Text = "TB";
                s.Type = "TB";

            }
            else if (s.Avg > 6 && s.Avg < 8)
            {
                txtType.Text = "Kha";
                s.Type = "Kha";
            }
            else if (s.Avg >= 8 && s.Avg <= 10)
            {
                txtType.Text = "Gioi";
                s.Type = "Gioi";
            }
            else
            {
                MessageBox.Show("Invalid");
            }
            studentRepository.UpdateStudent(s);
            LoadStudentList();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.Id = int.Parse(txtID.Text);
            s.Name = txtName.Text;
            s.Age = int.Parse(txtAge.Text);
            s.Address = txtAddress.Text;
            s.Avg = double.Parse(txtAvgMark.Text);
            if (msbYear.Text.Length < 4 || msbYear.Text.Length > 4)
            {
                MessageBox.Show("This field must be filled 4 characters.");
            }
            else
            {
                s.AdmissionalYear = int.Parse(msbYear.Text);
            }
            s.Majors = txtMajors.Text;

            if (s.Avg > 0 && s.Avg <= 4)
            {
                txtType.Text = "Yeu";
                s.Type = "Yeu";
            }
            else if (s.Avg > 4 && s.Avg < 6)
            {
                txtType.Text = "TB";
                s.Type = "TB";

            }
            else if (s.Avg >= 6 && s.Avg < 8)
            {
                txtType.Text = "Kha";
                s.Type = "Kha";
            }
            else if (s.Avg >= 8 && s.Avg <= 10)
            {
                txtType.Text = "Gioi";
                s.Type = "Gioi";
            }
            else
            {
                MessageBox.Show("Invalid");
            }
            studentRepository.DeleteStudent(s.Id);
            LoadStudentList();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            txtID.Text = string.Empty;
            txtName.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtAge.Text = string.Empty;
            txtMajors.Text = string.Empty;
            txtNameFilter.Text = string.Empty;
            txtMajorFilter.Text = string.Empty;
            msbYear.Text = "0";
            txtAvgMark.Text = string.Empty;
            txtType.Text = string.Empty;
            txtTypeFilter.Text = string.Empty;
            LoadStudentList();
        }

        private void txtNameFilter_TextChanged(object sender, EventArgs e)
        {
            string key = txtNameFilter.Text.Trim();
            dgvStudent.DataSource = db.Students.Where(s => s.Name.Contains(key)).ToList();
        }

        private void txtMajorFilter_TextChanged(object sender, EventArgs e)
        {
            string key = txtMajorFilter.Text.Trim();
            dgvStudent.DataSource = db.Students.Where(s => s.Majors.Contains(key)).ToList();
        }

        private void txtTypeFilter_TextChanged(object sender, EventArgs e)
        {
            string key = txtTypeFilter.Text.Trim();
            dgvStudent.DataSource = db.Students.Where(s => s.Type.Contains(key)).ToList();
        }

        private void btnAscID_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderBy(s => s.Id).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnDesID_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderByDescending(s => s.Id).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnAscName_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderBy(s => s.Name).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnDesName_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderByDescending(s => s.Id).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnAscAvg_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderBy(s => s.Avg).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnDesAvg_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderByDescending(s => s.Avg).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnAgeAsc_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderBy(s => s.Age).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnDesAge_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderByDescending(s => s.Age).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnYearAsc_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderBy(s => s.AdmissionalYear).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnDesYear_Click(object sender, EventArgs e)
        {
            using (var context = new StudentsContext())
            {
                var students = context.Students.OrderByDescending(s => s.AdmissionalYear).ToList();
                dgvStudent.DataSource = students;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
