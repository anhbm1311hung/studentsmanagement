﻿namespace StudentManagement
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbID = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lbName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lbAge = new System.Windows.Forms.Label();
            this.lbAddress = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lbMajors = new System.Windows.Forms.Label();
            this.txtMajors = new System.Windows.Forms.TextBox();
            this.lbAdmissionalYear = new System.Windows.Forms.Label();
            this.msbYear = new System.Windows.Forms.MaskedTextBox();
            this.lbAgeStudent = new System.Windows.Forms.Label();
            this.txtAge = new System.Windows.Forms.TextBox();
            this.txtAvgMark = new System.Windows.Forms.TextBox();
            this.lbType = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.dgvStudent = new System.Windows.Forms.DataGridView();
            this.gbFilter = new System.Windows.Forms.GroupBox();
            this.txtTypeFilter = new System.Windows.Forms.TextBox();
            this.lbTypeFilter = new System.Windows.Forms.Label();
            this.txtMajorFilter = new System.Windows.Forms.TextBox();
            this.lbMajorFilter = new System.Windows.Forms.Label();
            this.txtNameFilter = new System.Windows.Forms.TextBox();
            this.lbNameFilter = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.gbSort = new System.Windows.Forms.GroupBox();
            this.btnDesYear = new System.Windows.Forms.Button();
            this.btnYearAsc = new System.Windows.Forms.Button();
            this.btnDesAge = new System.Windows.Forms.Button();
            this.btnAgeAsc = new System.Windows.Forms.Button();
            this.btnDesAvg = new System.Windows.Forms.Button();
            this.btnAscAvg = new System.Windows.Forms.Button();
            this.btnDesName = new System.Windows.Forms.Button();
            this.btnAscName = new System.Windows.Forms.Button();
            this.btnDesID = new System.Windows.Forms.Button();
            this.btnAscID = new System.Windows.Forms.Button();
            this.lbYearSort = new System.Windows.Forms.Label();
            this.lbAgeSort = new System.Windows.Forms.Label();
            this.lbAvgSort = new System.Windows.Forms.Label();
            this.lbNameSort = new System.Windows.Forms.Label();
            this.lbIdSort = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).BeginInit();
            this.gbFilter.SuspendLayout();
            this.gbSort.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbID
            // 
            this.lbID.AutoSize = true;
            this.lbID.Location = new System.Drawing.Point(37, 42);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(79, 20);
            this.lbID.TabIndex = 0;
            this.lbID.Text = "Student ID";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(161, 35);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(125, 27);
            this.txtID.TabIndex = 1;
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Location = new System.Drawing.Point(37, 102);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(104, 20);
            this.lbName.TabIndex = 2;
            this.lbName.Text = "Student Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(161, 95);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(210, 27);
            this.txtName.TabIndex = 3;
            // 
            // lbAge
            // 
            this.lbAge.AutoSize = true;
            this.lbAge.Location = new System.Drawing.Point(37, 407);
            this.lbAge.Name = "lbAge";
            this.lbAge.Size = new System.Drawing.Size(72, 20);
            this.lbAge.TabIndex = 4;
            this.lbAge.Text = "Avg Mark";
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Location = new System.Drawing.Point(37, 218);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(62, 20);
            this.lbAddress.TabIndex = 6;
            this.lbAddress.Text = "Address";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(161, 211);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(210, 27);
            this.txtAddress.TabIndex = 7;
            // 
            // lbMajors
            // 
            this.lbMajors.AutoSize = true;
            this.lbMajors.Location = new System.Drawing.Point(37, 284);
            this.lbMajors.Name = "lbMajors";
            this.lbMajors.Size = new System.Drawing.Size(54, 20);
            this.lbMajors.TabIndex = 8;
            this.lbMajors.Text = "Majors";
            // 
            // txtMajors
            // 
            this.txtMajors.Location = new System.Drawing.Point(161, 277);
            this.txtMajors.Name = "txtMajors";
            this.txtMajors.Size = new System.Drawing.Size(210, 27);
            this.txtMajors.TabIndex = 9;
            // 
            // lbAdmissionalYear
            // 
            this.lbAdmissionalYear.AutoSize = true;
            this.lbAdmissionalYear.Location = new System.Drawing.Point(23, 340);
            this.lbAdmissionalYear.Name = "lbAdmissionalYear";
            this.lbAdmissionalYear.Size = new System.Drawing.Size(118, 20);
            this.lbAdmissionalYear.TabIndex = 10;
            this.lbAdmissionalYear.Text = "AdmissionalYear";
            // 
            // msbYear
            // 
            this.msbYear.Location = new System.Drawing.Point(161, 333);
            this.msbYear.Mask = "0000";
            this.msbYear.Name = "msbYear";
            this.msbYear.Size = new System.Drawing.Size(125, 27);
            this.msbYear.TabIndex = 11;
            this.msbYear.Text = "0";
            // 
            // lbAgeStudent
            // 
            this.lbAgeStudent.AutoSize = true;
            this.lbAgeStudent.Location = new System.Drawing.Point(37, 161);
            this.lbAgeStudent.Name = "lbAgeStudent";
            this.lbAgeStudent.Size = new System.Drawing.Size(36, 20);
            this.lbAgeStudent.TabIndex = 12;
            this.lbAgeStudent.Text = "Age";
            // 
            // txtAge
            // 
            this.txtAge.Location = new System.Drawing.Point(161, 154);
            this.txtAge.Name = "txtAge";
            this.txtAge.Size = new System.Drawing.Size(125, 27);
            this.txtAge.TabIndex = 13;
            // 
            // txtAvgMark
            // 
            this.txtAvgMark.Location = new System.Drawing.Point(131, 401);
            this.txtAvgMark.Name = "txtAvgMark";
            this.txtAvgMark.Size = new System.Drawing.Size(83, 27);
            this.txtAvgMark.TabIndex = 14;
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(246, 407);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(40, 20);
            this.lbType.TabIndex = 15;
            this.lbType.Text = "Type";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(310, 400);
            this.txtType.Name = "txtType";
            this.txtType.ReadOnly = true;
            this.txtType.Size = new System.Drawing.Size(85, 27);
            this.txtType.TabIndex = 16;
            // 
            // dgvStudent
            // 
            this.dgvStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudent.Location = new System.Drawing.Point(437, 95);
            this.dgvStudent.Name = "dgvStudent";
            this.dgvStudent.RowHeadersWidth = 51;
            this.dgvStudent.RowTemplate.Height = 29;
            this.dgvStudent.Size = new System.Drawing.Size(930, 400);
            this.dgvStudent.TabIndex = 17;
            this.dgvStudent.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvStudent_CellContentClick);
            // 
            // gbFilter
            // 
            this.gbFilter.Controls.Add(this.txtTypeFilter);
            this.gbFilter.Controls.Add(this.lbTypeFilter);
            this.gbFilter.Controls.Add(this.txtMajorFilter);
            this.gbFilter.Controls.Add(this.lbMajorFilter);
            this.gbFilter.Controls.Add(this.txtNameFilter);
            this.gbFilter.Controls.Add(this.lbNameFilter);
            this.gbFilter.Location = new System.Drawing.Point(437, 12);
            this.gbFilter.Name = "gbFilter";
            this.gbFilter.Size = new System.Drawing.Size(930, 77);
            this.gbFilter.TabIndex = 18;
            this.gbFilter.TabStop = false;
            this.gbFilter.Text = "Filter";
            // 
            // txtTypeFilter
            // 
            this.txtTypeFilter.Location = new System.Drawing.Point(730, 35);
            this.txtTypeFilter.Name = "txtTypeFilter";
            this.txtTypeFilter.Size = new System.Drawing.Size(125, 27);
            this.txtTypeFilter.TabIndex = 5;
            this.txtTypeFilter.TextChanged += new System.EventHandler(this.txtTypeFilter_TextChanged);
            // 
            // lbTypeFilter
            // 
            this.lbTypeFilter.AutoSize = true;
            this.lbTypeFilter.Location = new System.Drawing.Point(635, 38);
            this.lbTypeFilter.Name = "lbTypeFilter";
            this.lbTypeFilter.Size = new System.Drawing.Size(73, 20);
            this.lbTypeFilter.TabIndex = 4;
            this.lbTypeFilter.Text = "TypeFilter";
            // 
            // txtMajorFilter
            // 
            this.txtMajorFilter.Location = new System.Drawing.Point(421, 35);
            this.txtMajorFilter.Name = "txtMajorFilter";
            this.txtMajorFilter.Size = new System.Drawing.Size(125, 27);
            this.txtMajorFilter.TabIndex = 3;
            this.txtMajorFilter.TextChanged += new System.EventHandler(this.txtMajorFilter_TextChanged);
            // 
            // lbMajorFilter
            // 
            this.lbMajorFilter.AutoSize = true;
            this.lbMajorFilter.Location = new System.Drawing.Point(321, 38);
            this.lbMajorFilter.Name = "lbMajorFilter";
            this.lbMajorFilter.Size = new System.Drawing.Size(81, 20);
            this.lbMajorFilter.TabIndex = 2;
            this.lbMajorFilter.Text = "MajorFilter";
            // 
            // txtNameFilter
            // 
            this.txtNameFilter.Location = new System.Drawing.Point(113, 31);
            this.txtNameFilter.Name = "txtNameFilter";
            this.txtNameFilter.Size = new System.Drawing.Size(114, 27);
            this.txtNameFilter.TabIndex = 1;
            this.txtNameFilter.TextChanged += new System.EventHandler(this.txtNameFilter_TextChanged);
            // 
            // lbNameFilter
            // 
            this.lbNameFilter.AutoSize = true;
            this.lbNameFilter.Location = new System.Drawing.Point(46, 38);
            this.lbNameFilter.Name = "lbNameFilter";
            this.lbNameFilter.Size = new System.Drawing.Size(49, 20);
            this.lbNameFilter.TabIndex = 0;
            this.lbNameFilter.Text = "Name";
            // 
            // btnEdit
            // 
            this.btnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEdit.Location = new System.Drawing.Point(15, 466);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(94, 29);
            this.btnEdit.TabIndex = 19;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(115, 466);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(94, 29);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(215, 466);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(94, 29);
            this.btnDelete.TabIndex = 21;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(315, 466);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(94, 29);
            this.btnRefresh.TabIndex = 22;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // gbSort
            // 
            this.gbSort.Controls.Add(this.btnDesYear);
            this.gbSort.Controls.Add(this.btnYearAsc);
            this.gbSort.Controls.Add(this.btnDesAge);
            this.gbSort.Controls.Add(this.btnAgeAsc);
            this.gbSort.Controls.Add(this.btnDesAvg);
            this.gbSort.Controls.Add(this.btnAscAvg);
            this.gbSort.Controls.Add(this.btnDesName);
            this.gbSort.Controls.Add(this.btnAscName);
            this.gbSort.Controls.Add(this.btnDesID);
            this.gbSort.Controls.Add(this.btnAscID);
            this.gbSort.Controls.Add(this.lbYearSort);
            this.gbSort.Controls.Add(this.lbAgeSort);
            this.gbSort.Controls.Add(this.lbAvgSort);
            this.gbSort.Controls.Add(this.lbNameSort);
            this.gbSort.Controls.Add(this.lbIdSort);
            this.gbSort.Location = new System.Drawing.Point(1373, 12);
            this.gbSort.Name = "gbSort";
            this.gbSort.Size = new System.Drawing.Size(287, 483);
            this.gbSort.TabIndex = 23;
            this.gbSort.TabStop = false;
            this.gbSort.Text = "Sort";
            // 
            // btnDesYear
            // 
            this.btnDesYear.Location = new System.Drawing.Point(162, 384);
            this.btnDesYear.Name = "btnDesYear";
            this.btnDesYear.Size = new System.Drawing.Size(94, 29);
            this.btnDesYear.TabIndex = 14;
            this.btnDesYear.Text = "DES Year";
            this.btnDesYear.UseVisualStyleBackColor = true;
            this.btnDesYear.Click += new System.EventHandler(this.btnDesYear_Click);
            // 
            // btnYearAsc
            // 
            this.btnYearAsc.Location = new System.Drawing.Point(46, 384);
            this.btnYearAsc.Name = "btnYearAsc";
            this.btnYearAsc.Size = new System.Drawing.Size(94, 29);
            this.btnYearAsc.TabIndex = 13;
            this.btnYearAsc.Text = "ASC Year";
            this.btnYearAsc.UseVisualStyleBackColor = true;
            this.btnYearAsc.Click += new System.EventHandler(this.btnYearAsc_Click);
            // 
            // btnDesAge
            // 
            this.btnDesAge.Location = new System.Drawing.Point(162, 309);
            this.btnDesAge.Name = "btnDesAge";
            this.btnDesAge.Size = new System.Drawing.Size(94, 29);
            this.btnDesAge.TabIndex = 12;
            this.btnDesAge.Text = "DES Age";
            this.btnDesAge.UseVisualStyleBackColor = true;
            this.btnDesAge.Click += new System.EventHandler(this.btnDesAge_Click);
            // 
            // btnAgeAsc
            // 
            this.btnAgeAsc.Location = new System.Drawing.Point(46, 308);
            this.btnAgeAsc.Name = "btnAgeAsc";
            this.btnAgeAsc.Size = new System.Drawing.Size(94, 29);
            this.btnAgeAsc.TabIndex = 11;
            this.btnAgeAsc.Text = "ASC Age";
            this.btnAgeAsc.UseVisualStyleBackColor = true;
            this.btnAgeAsc.Click += new System.EventHandler(this.btnAgeAsc_Click);
            // 
            // btnDesAvg
            // 
            this.btnDesAvg.Location = new System.Drawing.Point(162, 231);
            this.btnDesAvg.Name = "btnDesAvg";
            this.btnDesAvg.Size = new System.Drawing.Size(94, 29);
            this.btnDesAvg.TabIndex = 10;
            this.btnDesAvg.Text = "DES Avg";
            this.btnDesAvg.UseVisualStyleBackColor = true;
            this.btnDesAvg.Click += new System.EventHandler(this.btnDesAvg_Click);
            // 
            // btnAscAvg
            // 
            this.btnAscAvg.Location = new System.Drawing.Point(46, 231);
            this.btnAscAvg.Name = "btnAscAvg";
            this.btnAscAvg.Size = new System.Drawing.Size(94, 29);
            this.btnAscAvg.TabIndex = 9;
            this.btnAscAvg.Text = "ASC Avg";
            this.btnAscAvg.UseVisualStyleBackColor = true;
            this.btnAscAvg.Click += new System.EventHandler(this.btnAscAvg_Click);
            // 
            // btnDesName
            // 
            this.btnDesName.Location = new System.Drawing.Point(162, 154);
            this.btnDesName.Name = "btnDesName";
            this.btnDesName.Size = new System.Drawing.Size(94, 29);
            this.btnDesName.TabIndex = 8;
            this.btnDesName.Text = "DES Name";
            this.btnDesName.UseVisualStyleBackColor = true;
            this.btnDesName.Click += new System.EventHandler(this.btnDesName_Click);
            // 
            // btnAscName
            // 
            this.btnAscName.Location = new System.Drawing.Point(46, 154);
            this.btnAscName.Name = "btnAscName";
            this.btnAscName.Size = new System.Drawing.Size(94, 29);
            this.btnAscName.TabIndex = 7;
            this.btnAscName.Text = "ASC Name";
            this.btnAscName.UseVisualStyleBackColor = true;
            this.btnAscName.Click += new System.EventHandler(this.btnAscName_Click);
            // 
            // btnDesID
            // 
            this.btnDesID.Location = new System.Drawing.Point(162, 79);
            this.btnDesID.Name = "btnDesID";
            this.btnDesID.Size = new System.Drawing.Size(94, 29);
            this.btnDesID.TabIndex = 6;
            this.btnDesID.Text = "DES ID";
            this.btnDesID.UseVisualStyleBackColor = true;
            this.btnDesID.Click += new System.EventHandler(this.btnDesID_Click);
            // 
            // btnAscID
            // 
            this.btnAscID.Location = new System.Drawing.Point(46, 79);
            this.btnAscID.Name = "btnAscID";
            this.btnAscID.Size = new System.Drawing.Size(94, 29);
            this.btnAscID.TabIndex = 5;
            this.btnAscID.Text = "ASC ID";
            this.btnAscID.UseVisualStyleBackColor = true;
            this.btnAscID.Click += new System.EventHandler(this.btnAscID_Click);
            // 
            // lbYearSort
            // 
            this.lbYearSort.AutoSize = true;
            this.lbYearSort.Location = new System.Drawing.Point(28, 348);
            this.lbYearSort.Name = "lbYearSort";
            this.lbYearSort.Size = new System.Drawing.Size(68, 20);
            this.lbYearSort.TabIndex = 4;
            this.lbYearSort.Text = "Sort Year";
            // 
            // lbAgeSort
            // 
            this.lbAgeSort.AutoSize = true;
            this.lbAgeSort.Location = new System.Drawing.Point(28, 273);
            this.lbAgeSort.Name = "lbAgeSort";
            this.lbAgeSort.Size = new System.Drawing.Size(67, 20);
            this.lbAgeSort.TabIndex = 3;
            this.lbAgeSort.Text = "Sort Age";
            // 
            // lbAvgSort
            // 
            this.lbAvgSort.AutoSize = true;
            this.lbAvgSort.Location = new System.Drawing.Point(28, 195);
            this.lbAvgSort.Name = "lbAvgSort";
            this.lbAvgSort.Size = new System.Drawing.Size(66, 20);
            this.lbAvgSort.TabIndex = 2;
            this.lbAvgSort.Text = "Sort Avg";
            // 
            // lbNameSort
            // 
            this.lbNameSort.AutoSize = true;
            this.lbNameSort.Location = new System.Drawing.Point(28, 121);
            this.lbNameSort.Name = "lbNameSort";
            this.lbNameSort.Size = new System.Drawing.Size(80, 20);
            this.lbNameSort.TabIndex = 1;
            this.lbNameSort.Text = "Sort Name";
            // 
            // lbIdSort
            // 
            this.lbIdSort.AutoSize = true;
            this.lbIdSort.Location = new System.Drawing.Point(28, 46);
            this.lbIdSort.Name = "lbIdSort";
            this.lbIdSort.Size = new System.Drawing.Size(55, 20);
            this.lbIdSort.TabIndex = 0;
            this.lbIdSort.Text = "Sort ID";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(674, 516);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(94, 29);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1672, 557);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.gbSort);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.gbFilter);
            this.Controls.Add(this.dgvStudent);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.lbType);
            this.Controls.Add(this.txtAvgMark);
            this.Controls.Add(this.txtAge);
            this.Controls.Add(this.lbAgeStudent);
            this.Controls.Add(this.msbYear);
            this.Controls.Add(this.lbAdmissionalYear);
            this.Controls.Add(this.txtMajors);
            this.Controls.Add(this.lbMajors);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lbAddress);
            this.Controls.Add(this.lbAge);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.lbID);
            this.Name = "Form1";
            this.Text = "Students Management";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).EndInit();
            this.gbFilter.ResumeLayout(false);
            this.gbFilter.PerformLayout();
            this.gbSort.ResumeLayout(false);
            this.gbSort.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lbID;
        private TextBox txtID;
        private Label lbName;
        private TextBox txtName;
        private Label lbAge;
        private Label lbAddress;
        private TextBox txtAddress;
        private Label lbMajors;
        private TextBox txtMajors;
        private Label lbAdmissionalYear;
        private MaskedTextBox msbYear;
        private Label lbAgeStudent;
        private TextBox txtAge;
        private TextBox txtAvgMark;
        private Label lbType;
        private TextBox txtType;
        private DataGridView dgvStudent;
        private GroupBox gbFilter;
        private TextBox txtTypeFilter;
        private Label lbTypeFilter;
        private TextBox txtMajorFilter;
        private Label lbMajorFilter;
        private TextBox txtNameFilter;
        private Label lbNameFilter;
        private Button btnEdit;
        private Button btnAdd;
        private Button btnDelete;
        private Button btnRefresh;
        private GroupBox gbSort;
        private Label lbIdSort;
        private Label lbAgeSort;
        private Label lbAvgSort;
        private Label lbNameSort;
        private Button btnDesYear;
        private Button btnYearAsc;
        private Button btnDesAge;
        private Button btnAgeAsc;
        private Button btnDesAvg;
        private Button btnAscAvg;
        private Button btnDesName;
        private Button btnAscName;
        private Button btnDesID;
        private Button btnAscID;
        private Label lbYearSort;
        private Button btnClose;
    }
}