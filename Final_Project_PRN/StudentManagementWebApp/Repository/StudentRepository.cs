﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentManagementWebApp.Models;

namespace StudentManagementWebApp.Repository
{
    public class StudentRepository : IStudentRepository
    {
        public Student GetStudentByID(int studentId) => StudentDAO.Instance.GetStudentByID(studentId);
        public IEnumerable<Student> GetStudents() => StudentDAO.Instance.GetStudentList();
        public void InsertStudent(Student student) => StudentDAO.Instance.AddNew(student);
        public void DeleteStudent(int studentId) => StudentDAO.Instance.Remove(studentId);
        public void UpdateStudent(Student student) => StudentDAO.Instance.Update(student);
    }
}
