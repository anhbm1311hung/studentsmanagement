﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagementWebApp.Repository;
using StudentManagementWebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace StudentsManagementWebApp.Controllers
{
    public class StudentsController : Controller
    {
        IStudentRepository studentRepository = null;
        public StudentsController() => studentRepository = new StudentRepository();
        // GET: StudentsController
        public ActionResult Index(string sortOrder)
        {
            var context = new StudentsContext();
            ViewBag.IdSortParm = String.IsNullOrEmpty(sortOrder) ? "id_desc" : "";
            ViewBag.AvgSortParm = sortOrder == "Avg" ? "avg_desc" : "Avg";
            ViewBag.AgeSortParm = sortOrder == "Age" ? "age_desc" : "Age";
            ViewBag.YearSortParm = sortOrder == "Year" ? "year_desc" : "Year";
            ViewBag.NameSortParm = sortOrder == "Name" ? "name_desc" : "Name";

            var students = from s in context.Students
                           select s;
            switch (sortOrder)
            {
                case "id_desc":
                    students = students.OrderByDescending(s => s.Id);
                    break;
                case "Avg":
                    students = students.OrderBy(s => s.Avg);
                    break;
                case "avg_desc":
                    students = students.OrderByDescending(s => s.Avg);
                    break;
                case "Age":
                    students = students.OrderBy(s => s.Age);
                    break;
                case "age_desc":
                    students = students.OrderByDescending(s => s.Age);
                    break;
                case "Year":
                    students = students.OrderBy(s => s.AdmissionalYear);
                    break;
                case "year_desc":
                    students = students.OrderByDescending(s => s.AdmissionalYear);
                    break;
                case "Name":
                    students = students.OrderBy(s => s.Name);
                    break;
                case "name_desc":
                    students = students.OrderByDescending(s => s.Name);
                    break;
                default:
                    students = students.OrderBy(s => s.Id);
                    break;
            }
            return View(students.ToList());
        }

        public ActionResult SearchByName(string name)
        {
            var context = new StudentsContext();
            var students = from s in context.Students
                           where s.Name.Contains(name)
                           select s;
            return View("Index", students);
        }

        public ActionResult SearchByMajor(string major)
        {
            var context = new StudentsContext();
            var students = from s in context.Students
                           where s.Majors.Contains(major)
                           select s;
            return View("Index", students);
        }

        public ActionResult SearchByType(string type)
        {
            var context = new StudentsContext();
            var students = from s in context.Students
                           where s.Type.Contains(type)
                           select s;
            return View("Index", students);
        }

        // GET: StudentsController/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var student = studentRepository.GetStudentByID(id.Value);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // GET: StudentsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StudentsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Student student)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    studentRepository.InsertStudent(student);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View(student);
            }
        }

        // GET: StudentsController/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var student = studentRepository.GetStudentByID(id.Value);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: StudentsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Student student)
        {
            try
            {
                if (id != student.Id)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    studentRepository.UpdateStudent(student);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
        }

        // GET: StudentsController/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var student = studentRepository.GetStudentByID(id.Value);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: StudentsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            try
            {
                studentRepository.DeleteStudent(id);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = ex.Message;
                return View();
            }
        }
    }
}
